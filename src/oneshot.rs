use crate::ValidatedSignedOperation;
use gloo_console::log;
use gloo_net::http::Request;
use gloo_worker::{HandlerId, Worker, WorkerScope};
use serde::{Deserialize, Serialize};

// HEY YEW
//
// yew is really immature and regularly make breaking changes
// if this stops working, the library has likely changed
// if this is still happening in 2024, then probably should drop yew
// ANYWAY: writing this sucked as much as it did the first three times yew changed everything
// rant over
//
// lol just switched to gloo_worker and at least it's functional
// leaving this here for a while because i'm still mad at yew

#[derive(Serialize, Deserialize)]
pub struct WorkerInput {
    #[serde(skip)]
    pub after: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct WorkerOutput {
    pub ops: Vec<ValidatedSignedOperation>,
}

pub struct WebWorker {}

impl Worker for WebWorker {
    type Input = WorkerInput;
    type Output = WorkerOutput;
    type Message = ();

    fn create(_scope: &WorkerScope<Self>) -> Self {
        log!("init wrkr");
        Self {}
    }

    fn received(&mut self, scope: &WorkerScope<Self>, msg: Self::Input, id: HandlerId) {
        // this runs in a web worker
        // and does not block the main
        // browser thread!
        let scope = scope.clone();
        log!("rx msg");
        match msg {
            WorkerInput { after: _ts } => {
                log!("matched on msg");
                wasm_bindgen_futures::spawn_local(async move {
                    let operations = fetch_operations(None).await;
                    scope.respond(
                        id,
                        WorkerOutput { ops: operations.clone() },
                    );
                })
            }
        }
    }

    fn connected(&mut self, _scope: &WorkerScope<Self>, _id: HandlerId) {}

    fn update(&mut self, _scope: &WorkerScope<Self>, _msg: Self::Message) {}
}

async fn fetch_operations(_after: Option<String>) -> Vec<ValidatedSignedOperation> {
    let response = Request::get("https://plc.directory/export").send().await;
    let mut operations: Vec<ValidatedSignedOperation> = Vec::new();

    match response {
        Ok(body) => match body.status() {
            200 => match body.text().await {
                Ok(text) => {
                    let lines: Vec<&str> = text.lines().collect();
                    for line in lines {
                        let result: Result<ValidatedSignedOperation, serde_json::Error> =
                            serde_json::from_str(line);
                        match result {
                            Ok(validated_signed_operation) => {
                                operations.push(validated_signed_operation);
                            }
                            Err(e) => log!("Failed to deserialize a line: ", e.to_string()),
                        }
                    }
                    log!("Successfully deserialized {} objects.", operations.len());
                }
                Err(e) => log!("Error: Failed to read text: {}", e.to_string()),
            },
            status_code => {
                log!("Error: Received unexpected status code: {}", status_code)
            }
        },
        Err(e) => log!("Error: Failed to fetch data: {}", e.to_string()),
    }

    operations
}
