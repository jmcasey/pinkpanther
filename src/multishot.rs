use crate::ValidatedSignedOperation;
use futures_util::sink::SinkExt;
use futures_util::StreamExt;
use gloo_console::log;
use gloo_net::http::Request;
use gloo_worker::reactor::reactor;
use gloo_worker::reactor::ReactorScope;
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
#[serde(crate = "serde")]
pub enum Input {
    Start(Option<u32>, Option<String>),
    Stop,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
#[serde(crate = "serde")]
pub enum Output {
    FetchedOperation(ValidatedSignedOperation, String),
    Stop,
}

#[reactor]
pub async fn WebReactor(mut scope: ReactorScope<Input, Output>) {
    while let Some(control_signal) = scope.next().await {
        log!("called reactor");
        match control_signal {
            Input::Start(n, after) => {
                log!("reactor start");
                output_n_operations(&mut scope, n, after).await;
            }
            Input::Stop => break,
        }
    }
}

pub async fn output_n_operations(scope: &mut ReactorScope<Input, Output>, n: Option<u32>, after: Option<String>) {
    let mut req = Request::get("https://plc.directory/export");

    if let Some(ts) = after {
        req = req.query([("after", &ts)]);
    }

    if let Some(n) = n {
        req = req.query([("count", &n.to_string())]);
    }
    let req = req.build().unwrap();
    let response = req.send().await;

    match response {
        Ok(body) => match body.status() {
            200 => match body.text().await {
                Ok(text) => {
                    let lines: Vec<&str> = text.lines().collect();
                    for line in lines {
                        let result: Result<ValidatedSignedOperation, serde_json::Error> =
                            serde_json::from_str(line);
                        match result {
                            Ok(op) => {
                                let ts = op.created_at.clone();
                                scope
                                    .send(Output::FetchedOperation(op, ts))
                                    .await
                                    .unwrap();
                            }
                            Err(e) => log!("Failed to deserialize a line: ", e.to_string()),
                        }
                    }
                }
                Err(e) => log!("Error: Failed to read text: {}", e.to_string()),
            },
            status_code => {
                log!("Error: Received unexpected status code: {}", status_code)
            }
        },
        Err(e) => log!("Error: Failed to fetch data: {}", e.to_string()),
    }
    scope
        .send(Output::Stop)
        .await
        .unwrap();
}


async fn fetch_single_operation(after: Option<String>) -> Option<ValidatedSignedOperation> {
    let url = if let Some(ts) = after {
        format!("https://plc.directory/export?after={}", ts)
    } else {
        "https://plc.directory/export".to_string()
    };

    let response = Request::get(&url)
        .send()
        .await;

    match response {
        Ok(body) => match body.status() {
            200 => match body.text().await {
                Ok(text) => {
                    let result: Result<ValidatedSignedOperation, serde_json::Error> =
                        serde_json::from_str(&text);
                    match result {
                        Ok(validated_signed_operation) => {
                            return Some(validated_signed_operation);
                        }
                        Err(e) => log!("Failed to deserialize a line: ", e.to_string()),
                    }
                }
                Err(_) => todo!(),
            },
            status_code => {
                log!("Error: Received unexpected status code: {}", status_code)
            }
        },
        Err(e) => log!("Error: Failed to fetch data: {}", e.to_string()),
    }
    unreachable!();
}

async fn fetch_operations(_after: Option<String>) -> Vec<ValidatedSignedOperation> {
    let response = Request::get("https://plc.directory/export").send().await;
    let mut operations: Vec<ValidatedSignedOperation> = Vec::new();

    match response {
        Ok(body) => match body.status() {
            200 => match body.text().await {
                Ok(text) => {
                    let lines: Vec<&str> = text.lines().collect();
                    for line in lines {
                        let result: Result<ValidatedSignedOperation, serde_json::Error> =
                            serde_json::from_str(line);
                        match result {
                            Ok(validated_signed_operation) => {
                                operations.push(validated_signed_operation);
                            }
                            Err(e) => log!("Failed to deserialize a line: ", e.to_string()),
                        }
                    }
                    log!("Successfully deserialized {} objects.", operations.len());
                }
                Err(e) => log!("Error: Failed to read text: {}", e.to_string()),
            },
            status_code => {
                log!("Error: Received unexpected status code: {}", status_code)
            }
        },
        Err(e) => log!("Error: Failed to fetch data: {}", e.to_string()),
    }

    operations
}
