use std::rc::Rc;
use yew::html;
use yew::Callback;
use crate::routes::Route;
use yew_router::prelude::*;
use yew::Html;
use stylist::yew::styled_component;
use stylist::style;

#[styled_component(NavBar)]
pub fn nav_bar() -> Html {
    let navigator = Rc::new(use_navigator().unwrap());

    let nav_home = {
        let nav_clone = navigator.clone();
        Callback::from(move |_| nav_clone.push(&Route::Home))
    };

    let nav_operations = {
        let nav_clone = navigator.clone();
        Callback::from(move |_| nav_clone.push(&Route::Operations))
    };

    let nav_post = {
        let nav_clone = navigator.clone();
        Callback::from(move |_| nav_clone.push(&Route::Post))
    };

    let nav_style = style! {
        r#"
        display: flex;
        justify-content: space-around;
        padding: 16px 0;
        background-color: #fff;
        border-top: 1px solid #e0e0e0;
        "#
    }.expect("Failed to create nav style.");

    let button_style = style! {
        r#"
        background-color: #FFB6C1;
        border: none;
        color: white;
        padding: 12px 24px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        border-radius: 4px;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.16), 0 2px 10px rgba(0, 0, 0, 0.12);
        transition: background-color 0.3s, transform 0.3s, box-shadow 0.3s;

        &:hover {
            background-color: #3700b3;
            cursor: pointer;
        }

        &:active {
            transform: scale(0.98);
            box-shadow: 0 4px 5px rgba(0, 0, 0, 0.2);
        }

        &:focus {
            outline: none;
            box-shadow: 0 0 0 3px rgba(98, 0, 234, 0.4);
        }
        "#r
    }.expect("Failed to create button style.");


    html! {
        <div class={ nav_style }>
            <button onclick={nav_home} class={ button_style.clone() }>{ "Home" }</button>
            <button onclick={nav_operations} class={ button_style.clone() }>{ "Operations" }</button>
            <button onclick={nav_post} class={ button_style.clone() }>{ "Create+" }</button>
        </div>
    }
}