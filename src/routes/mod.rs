

use yew_router::Routable;
use yew::html;


pub mod login;
pub mod settings;
pub mod home;
pub mod operations;
pub mod nav_bar;
pub mod post;

#[derive(Clone, Debug, Routable, PartialEq)]
pub enum Route {
    #[at("/")]
    Home,
    #[at("/login")]
    Login,
    #[at("/post")]
    Post,
    #[at("/operations")]
    Operations,
    #[at("/settings")]
    Settings,

    #[at("/to")]
    TermsOfService,
    #[at("/privacy")]
    Privacy,

    #[not_found]
    #[at("/404")]
    NotFound,
}