use crate::routes::nav_bar::NavBar;
use stylist::style;
use yew_router::prelude::*;
use crate::routes::Route;
use futures_util::StreamExt;
use gloo_worker::Spawnable;
use crate::multishot::Output;
use crate::ValidatedSignedOperation;
use crate::multishot::WebReactor;
use crate::multishot::Input;
use gloo_console::log;
use yew::html;
use yew::prelude::*;
use stylist::yew::styled_component;

#[styled_component(Operations)]
pub fn ops_list() -> Html {   

    let state_handle: UseStateHandle<Vec<ValidatedSignedOperation>> = use_state(|| vec!());

    let mut bridge = WebReactor::spawner().spawn("/reactor.js");
    bridge.send_input(Input::Start(Some(10), None));
    let state = state_handle.clone();

    use_effect_with_deps(move |_| {
        wasm_bindgen_futures::spawn_local(async move {
            log!("inside spawn_local");
            let mut ops: Vec<ValidatedSignedOperation> = vec!();
            loop {
                match bridge.next().await {
                    Some(Output::FetchedOperation(op, _ts)) => {
                        log!("got an operation");
                        ops.push(op);
                    },
                    _ => {
                        state.set(ops);
                        bridge.send_input(Input::Stop);
                        log!("out");
                        break;
                    },
                }
            }
        });
        || ()
    }, ());

    let navigator = use_navigator().unwrap();


    let card_style = style! {
        r#"
        display: flex;
        flex-direction: column;
        padding: 16px;
        margin: 16px 0;
        border-radius: 8px;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1), 0 2px 10px rgba(0, 0, 0, 0.1);
        background-color: #eee;
        transition: transform 0.2s;

        &:hover {
            transform: translateY(-3px);
        }
        "#
    }.expect("Failed to create card style.");

    let card_content_style = style! {
        r#"
        display: flex;
        flex-direction: column;
        gap: 8px;
        "#
    }.expect("Failed to create card content style.");

    let subtle_text_style = style! {
        r#"
        color: #757575;
        "#
    }.expect("Failed to create subtle text style.");

    let content_style = style! {
        r#"
            display: flex;
            flex-direction: column;
            border: 1px solid #e0e0e0;
            background-color: #ffffff;
            padding: 32px;
            border-radius: 8px;
            min-height: 66vh;
        "#
    }.expect("Failed to create desktop content style.");

    let operations_style = style! {
        display: "flex";
        flex_direction: "column";
        gap: "20px";
    }.expect("Failed to create operations style.");

    let button_style = style! {
        align_self: "flex-end";
        padding: "10px 20px";
        border_radius: "5px";
        background_color: "#FFB6C1";
        color: "#fff";
        cursor: "pointer";
        transition: "background-color 0.2s";
        ":hover": {
            background_color: "#FFA0B1";
        }
    }.expect("Failed to create button style.");

    let input_style = style! {
        padding: "10px";
        border: "1px solid #ddd";
        border_radius: "5px";
        outline: "none";
        ":focus": {
            border_color: "#FFB6C1";
        }
    }.expect("Failed to create input style.");

    log!("rendering component");
    let fallback = html! { <div>{ "Fetching operations..." }</div> };
    html! {
    <Suspense {fallback}>
        <div class={ content_style }>
            <h2> { "DID:PLC Operations" } </h2>
            
            <br /> { "This tool allows you to retrieve records from plc.directory" }
            <p /> { "DID:PLC operations are stored chronologically, and results are returned starting at \"the begining of time\". If you would like to start from another point in time, use the calendar below. The server will return a maximum of 1000 records, though it will take the DOM a moment to update if you pull that many at once." }
            <p> { "Pulling initial records will show you the first accounts to be created on the network. By pulling the audit log for " } <code> {"did:plc:egpxmsya32mhhh5smd375vcx"}</code> { ", you can see when I first joined." } </p>

            // Fetch first n operations
            <div>
                <input id="first_n" class={ input_style.clone() } type="number" placeholder="# of Operations" />
                <button class={ button_style.clone() }>{ "Fetch from start" }</button>
            </div>
            
            // Fetch latest operations after timestamp
            <div>
                <label for="after"> { "Fetch operations after: " } </label>
                <input id="after" class={ input_style.clone() } type="date" placeholder="Enter number of operations" />
                <button class={ button_style.clone() }>{ "Fetch from date" }</button>
            </div>
            
            // Fetch document for identifier
            <div>
                <input id="doc_id" class={ input_style.clone() } type="text" placeholder="did:plc:ewvi7nxzyoun6zhxrhs64oiz" />
                <button class={ button_style.clone() }>{ "Fetch DID Document" }</button>
                <button class={ button_style.clone() }>{ "Fetch Audit Log" }</button>
            </div>
            <div style="flex-grow: 1;"></div> // Add this line to push the NavBar to the bottom
            <NavBar />
        </div>
        {
            for state_handle.iter().rev().map(|op| html! {
                <div class={ card_style.clone() }>
                    <div class={ card_content_style.clone() }>
                        { &op.did() }<br/>
                        <span class={ subtle_text_style.clone() }>{ &op.created_at() }</span><br/>
                        { "Username: " } { &op.username().unwrap_or("N/A".to_string()) }<br/>
                        { "PDS Host: " } { &op.atp_pds().unwrap_or("N/A".to_string()) }<br/>
                    </div>
                </div>
            })
        }
    </Suspense>
}
}