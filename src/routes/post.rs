use crate::routes::nav_bar::NavBar;
use stylist::style;
use futures_util::StreamExt;
use stylist::yew::styled_component;

use crate::multishot::Output;
use crate::routes::Route;
use crate::ValidatedSignedOperation;
use crate::multishot::WebReactor;

use yew::prelude::*;
use yew_router::prelude::*;
use yew::Html;
use yew::function_component;
use crate::routes::html;




#[styled_component(Post)]
pub fn post() -> Html {
    let navigator = use_navigator().unwrap();

    //let nav_operations = Callback::from(move |_|
    //    navigator.push(&Route::Operations)
   // );

    let content_style = style! {
    r#"
        display: flex;
        flex-direction: column;
        border: 1px solid #e0e0e0;
        background-color: #ffffff;
        padding: 32px;
        border-radius: 8px;
        min-height: 66vh;
    "#
    }.expect("Failed to create desktop content style.");

    let placeholder = r#"{
      type: 'plc_operation',
      verificationMethods: {
        atproto: "did:key:zSigningKey"
      },
      rotationKeys: [
        "did:key:zRecoveryKey",
        "did:key:zRotationKey"
      ],
      alsoKnownAs: [
        "at://alice.test"
      ],
      services: {
        atproto_pds: {
          type: "AtprotoPersonalDataServer",
          endpoint: "https://example.test"
        }
      },
      prev: null,
      sig: 'sig_from_did:key:zRotationKey'
}"#;

    html! {
        <div class={ content_style }>
            <h2>{ "Submit DID:PLC Operation" } </h2>
            <textarea placeholder={ placeholder } style="width: 100%; height: 360px;"></textarea>
            <button type="submit">{ "Submit" }</button>
            <div style="flex-grow: 1;"></div> // Add this line to push the NavBar to the bottom
            <NavBar />
        </div>
    }
}