use stylist::style;
use crate::routes::nav_bar::NavBar;
use stylist::yew::styled_component;
use yew_router::prelude::*;
use yew::Html;
use crate::routes::html;

#[styled_component(Home)]
pub fn home() -> Html {
    let _navigator = use_navigator().unwrap();

    //let nav_operations = Callback::from(move |_|
    //    navigator.push(&Route::Operations)
   // );

    let content_style = style! {
            r#"
        display: flex;
        flex-direction: column;
        border: 1px solid #e0e0e0;
        background-color: #ffffff;
        padding: 32px;
        border-radius: 8px;
        min-height: 66vh;
    "#
    }.expect("Failed to create desktop content style.");

    html! {
        <div class={ content_style }>
            <h1>{ "PinkPanther: A DID:PLC Auditor" } </h1>
            <p> { "Dive into the world of DID PLC - a DID method that's both strongly-consistent and recoverable." } </p>
            <p> { ""}<a href="https://blueskyweb.xyz">{"Bluesky PBC"}</a>{" developed DID PLC when designing the "}<a href="https://atproto.com/">{"AT Protocol"}</a>{" (atproto) because they were not satisfied with any of the existing DID methods. They wanted a strongly consistent, highly available, recoverable, and cryptographically secure method with fast and cheap propagation of updates." } </p>
            <p> { "It was originally titled the method \"placeholder\", because the dev team didn't want it to stick around forever in its current form. They aim to replace it with, or evolve it into something less centralized - likely a permissioned DID consortium." } </p>
            <p> { "A DID:PLC identifier looks like this: "}<a href="https://bsky.app/profile/did:plc:ewvi7nxzyoun6zhxrhs64oiz">{"did:plc:ewvi7nxzyoun6zhxrhs64oiz"}</a>{"." } </p>
            <p> { "A dedicated directory server ("}<a href="https://plc.directory">{"plc.directory"}</a>{") is currently in place to ensure each operation is both ordered and validated, and allows auditing via a transparent log. For an in-depth dive, check out the official " }<a href="https://github.com/did-method-plc/did-method-plc">{"GitHub repository"}</a>{ "." } </p>
            <p> { " In the meantime, projects like PinkPanther allow interested parties to validate that plc.directory is operating benevolantly." } </p>
            <p />
        <div style="flex-grow: 1;"></div> // pushes the NavBar to the bottom
        <NavBar />
                </div>
    }
}