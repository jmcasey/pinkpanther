use gloo_worker::Registrable;
use pinkpanther::oneshot::WebWorker;

fn main() {
    WebWorker::registrar().register();
}
