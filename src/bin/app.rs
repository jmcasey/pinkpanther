use pinkpanther::routes::post::Post;
use stylist::style;
use pinkpanther::routes::operations::Operations;
use pinkpanther::routes::home::Home;
use pinkpanther::routes::Route;
use yew_router::BrowserRouter;
use yew_router::Switch;
use yew::prelude::*;

fn main() {
    yew::Renderer::<crate::App>::new().render();
}

#[function_component]
pub fn App() -> Html {

    let base_style = style! {
        r#"
        font-family: "Arial", sans-serif;
        margin: 16px;
        padding: 16px;
        "#
    }.expect("Failed to create app container style.");

    let _cloud = style!{
        r#"
        #cloud {
          background: rgba(255, 255, 255);
          width: 300px; 
          height: 100px;
          border-radius: 150px; 
          box-shadow: 10px 10px rgba(0,0,0,0.2);
          animation: move 3s infinite;
        }

        #cloud:after {
          content: '';
          background: rgba(255, 255, 255);
          position: absolute;
          width: 100px;
          height: 100px;
          border-radius: 50%;
          top: -50px;
          left: 50px;
        }

        #cloud:before {
          content: '';
          background: rgba(255, 255, 255);
          position: absolute;
          width: 170px;
          height: 150px;
          border-radius: 50%;
          top: -90px;
          right: 40px;
        }
        "#r
    }.expect("Failed to create cloud.");

    
    let desktop_content_style = style! {
        r#"
        @media (min-width: 768px) {
            width: 60%;
            margin: 0 auto;
        }
        "#
    }.expect("Failed to create desktop content style.");
    

    html! {
        <div class={ base_style }>
            //<div class={ cloud }>
                <div class={ desktop_content_style }>
                    <BrowserRouter>
                        <Switch<Route> render={switch} />
                    </BrowserRouter>
                </div>
            //</div>
        </div>
    }
}

fn switch(routes: Route) -> Html {
    match routes {
        Route::Home => html! { <Home /> },
        Route::Operations => html! { <Operations /> },
        Route::Post => html! { <Post /> },
        Route::NotFound => html! { <h1>{ "404" }</h1> },
        _ => html! { <h1>{ "Not yet implemented" }</h1> }
    }
}