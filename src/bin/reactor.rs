use gloo_worker::Registrable;
use pinkpanther::multishot::WebReactor;

fn main() {
    WebReactor::registrar().register();
}
