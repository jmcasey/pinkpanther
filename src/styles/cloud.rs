use stylist::Style;
use stylist::style;

let cloud = style!{
        r#"
        #cloud {
          background: rgba(255, 255, 255);
          width: 300px; 
          height: 100px;
          border-radius: 150px; 
          box-shadow: 10px 10px rgba(0,0,0,0.2);
          animation: move 3s infinite;
        }

        #cloud:after {
          content: '';
          background: rgba(255, 255, 255);
          position: absolute;
          width: 100px;
          height: 100px;
          border-radius: 50%;
          top: -50px;
          left: 50px;
        }

        #cloud:before {
          content: '';
          background: rgba(255, 255, 255);
          position: absolute;
          width: 170px;
          height: 150px;
          border-radius: 50%;
          top: -90px;
          right: 40px;
        }
        "#r
}.expect("Failed to create cloud.");