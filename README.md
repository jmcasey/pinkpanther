# PinkPanther 🐾

pinkpanther is a WASM web application template built using the Yew framework and gloo_worker for access to [web workers](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Using_web_workers). This project aims to serve as a robust starting point for my WASM apps, filled with snazziness right out of the box. No more boilerplate, just straight-up development.

Since I am still learning Rust, I advise against actually using this. While it should compile and deploy without issue, it is under development and not all features are implemented.

## Features 🚀

 - **Yew Integration**: Built on the Yew framework for high-performance web apps.
 - **Web Workers**: Leverage the power of multithreading.
 - **High Performance**: Not one single line of javascript will be commited to this repository.
 - **Streamlined Workflow**: Benefit from hot-reloading and state management capabilities via Trunk.

## Quick Start 🏁

1. **Clone the Repository**

    ```bash
    git clone https://gitlab.com/jmcasey/pinkpanther.git
    cd pinkpanther
    ```

2. **Install Dependencies**

    ```bash
    cargo install wasm-pack
    ```

3. **Run the Project**

    ```bash
    trunk serve --port 31337 --proxy-backend=https://plc.directory/export
    ```

Visit http://localhost:31337 and weep, for there are no more commands to run.

## Project Structure 🗃️
```
pinkpanther/
├── Cargo.lock
├── Cargo.toml
├── index.html
├── LICENSE
├── README.md
└── src
- ├── lib.rs
- ├── multishot.rs
- └── oneshot.rs
├── bin
- ├── app.rs
- ├── reactor.rs
- └── worker.rs
```

## Roadmap 📚
   
1. **PWA Support**: Enhance offline capabilities using Service Workers.

2. **Web Component Integration**: Looking at Bootstrap/Patternfly, open to other options.

3. **Dockerization**: Make deployment a breeze with a Dockerfile.
  
4. **CI/CD**: Set up a CI/CD pipeline using GitHub Actions or GitLab CI for automated testing and deployment.

5. **Analytics**: Integrate analytics tools to monitor user activity and performance.


## Contributions 💖

Contributions, issues, and feature requests are welcome! Feel free to check the issues page.

## License 📄

This project is licensed under the GPLv3 License - see the LICENSE file for details.
